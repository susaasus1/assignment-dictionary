section .rodata

%include 'colon.inc'

section .rodata
%include 'words.inc'
%include 'lib.inc'


error:
    db 'Ключ не найден', 10, 0

section .text

global _start

extern find_word

_start:
    sub rsp, 256
    mov rsi, 256
    mov rdi, rsp
    call read_word
    cmp rax, 0
    jz .not_found
    mov rdi, rsp
    mov rsi, first
    call find_word
    add rsp, 256
    cmp rax, 0
    jz .not_found
    add rax, 8
    mov rdi, rax
    push rax
    call string_length
    pop rsi
    add rsi, rax
    inc rsi
    mov rdi, 1
    call print_string
    call print_newline
    call exit
.not_found:
    add rsp, 256
    mov rdi, 2
    mov rsi, error
    call print_string
    call exit
