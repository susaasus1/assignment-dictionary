asm := nasm
asmflags := -felf64 -g
main := main
srcs := $(wildcard *.asm)
objs := $(patsubst %.asm,%.o,$(srcs))

.PHONY: all
all: $(main)
%.o: %.asm
        $(asm) $(asmflags) $< -o $@
$(main) : $(objs)
        ld $^ -o $@
