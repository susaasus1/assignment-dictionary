section .text

global exit
global string_length
global print_string
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy

; Принимает код возврата и завершает текущий процесс
exit:
    mov rax, 60
    syscall
    ret


; Принимает указатель на нуль-терминированную строку, возвращает её длину

string_length:
    xor rax, rax
.counter:
    cmp byte [rdi+rax], 0
    je .end
    inc rax
    jmp .counter
.end:
    ret


; Принимает дескриптор и указатель на нуль-терминированную строку, выводит строку в указанный дескриптор
print_string:
    push rdi
    push rsi
    mov rdi, rsi
    call string_length
    pop rsi
    pop rdi
    mov rdx, rax
    mov rax, 1
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
        push rdi
        mov rax, 1
        mov rdi, 1
        mov rsi, rsp
        mov rdx, 1
        syscall
        pop rdi
        ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 0xA
    jmp print_char


; Выводит беззнаковое 8-байтовое число в десятичном формате
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    xor rax, rax
    mov rax, rdi
    mov r10, 10
    mov rcx, rsp
    dec rcx
    sub rsp, 24
.loop:
    xor rdx, rdx
    div r10
    dec rcx
    add rdx, '0'
    mov [rcx], dl
    cmp rax, 0
    je .end
    jmp .loop
.end:
    mov rdi, rcx
    call print_string
    add rsp, 24
    ret


; Выводит знаковое 8-байтовое число в десятичном формате
print_int:
    cmp rdi, 0
    jnl .plusnumber
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    neg rdi
.plusnumber:
    call print_uint
    ret


; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rcx, rcx
.loop:
    xor r10, r10
    xor r11, r11
    mov r10b, byte[rdi+rcx]
    mov r11b, byte[rsi+rcx]
    cmp r10, r11
    jne .notequals
    cmp byte[rdi+rcx], 0
    je .equals
    inc rcx
    jmp .loop
.equals:
    mov rax, 1
    ret
.notequals:
    xor rax, rax
    ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rdi, rdi
    xor rax, rax
    mov rdx, 1
    push qword 0
    mov rsi, rsp
    syscall
    pop rax
    ret


; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
        xor r9, r9
        mov r10, rdi
.loop:
        push rdi
        push rsi
        call read_char
        pop rsi
        pop rdi
        cmp r9, 0
        jz .space
.next:
        cmp r9, rsi
        jae .buferr
        cmp rax, 0
        jz .end
        mov [rdi + r9], rax

        cmp rax, 0x9
        jz .end
        cmp rax, 0xa
        jz .end
        cmp rax, 0x20
        jz .end

        inc r9
        jmp .loop

.space:
        cmp rax, 0x9
        jz .loop
        cmp rax, 0xa
        jz .loop
        cmp rax, 0x20
        jz .loop
        jmp .next

.buferr:
        xor rax, rax
        ret
.end:
        xor rax, rax
        mov [rdi + r9], rax
        mov rax, r10
        mov rdx, r9
        ret


; Принимает: адрес начала буфера, размер буфера
; Читает в буфер строку из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Перевод строки не в начале завершает ввод строки
; Останавливается и возвращает 0 если строка слишком большая для буфера
; При успехе возвращает адрес буфера в rax, длину строки в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к строке нуль-терминатор


; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    mov r8, 10
    xor rcx, rcx
    xor rax, rax
.loop:
    movzx r9, byte[rdi + rcx]
    cmp r9b, 0
    je .end
    cmp r9b, '0'
    jb .end
    cmp r9b, '9'
    ja .end
    mul r8
    sub r9b, '0'
    add rax, r9
    inc rcx
    jmp .loop
.end:
    mov rdx, rcx
    ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был)
; rdx = 0 если число прочитать не удалось
parse_int:
    movzx r9, byte[rdi]
    cmp r9b, '-'
    je .negative
    push 0
    jmp .parse
.negative:
    push 1
    inc rdi

.parse:
    call parse_uint
    pop r9
    cmp r9, 0
    je .positive
    neg rax
    inc rdx
.positive:
    ret


; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    cmp rdx, 0
    je .bufzero
    xor r8, r8
    mov r8b, byte[rdi]
    mov byte[rsi], r8b
    inc rdi
    inc rsi
    dec rdx
    cmp r8, 0
    jne string_copy
    ret
.bufzero:
    xor rax, rax
    ret
