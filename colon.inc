%define first 0

%macro colon 2
    %2:
    dq first
    %define first %2
    db %1, 0
%endmacro
